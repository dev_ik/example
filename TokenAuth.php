<?php

namespace app\components\filters;

use yii\filters\auth\QueryParamAuth;
use yii\web\IdentityInterface;
use yii\web\Request;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\web\User;

class TokenAuth extends QueryParamAuth
{
    public const WIDGET_TOKEN = 'widget';

    public const API_TOKEN = 'api';

    /**
     * Authenticates the current user.
     * @param User $user
     * @param Request $request
     * @param Response $response
     * @return IdentityInterface the authenticated user identity. If authentication information is not provided, null will be returned.
     * @throws UnauthorizedHttpException if authentication information is provided but is invalid.
     */
    public function authenticate($user, $request, $response) : IdentityInterface
    {
        /** @var \app\models\User $identity */
        if (!$user->isGuest) {
            $identity = $user->identity;
            return !$identity->company->active ? $this->handleFailure($response) : $identity;
        }

        $accessToken = $request->get($this->tokenParam) ?? $request->post($this->tokenParam);
        if ($accessToken === null) {
            return null;
        }

        $identity = $user->loginByAccessToken($accessToken, self::API_TOKEN);
        if ($identity === null) {
            $identity = $user->loginByAccessToken($accessToken, self::WIDGET_TOKEN);
        }

        if ($identity === null) {
            return null;
        }

        if (!$identity->company->active || !$identity->active) {
            $this->handleFailure($response);
        }

        return $identity;
    }
}