<?php

namespace app\components\Intake;

/**
 * Interface KladrApiInterface
 * @package app\components\Intake
 */
interface KladrApiInterface
{

    /**
     * @param string $type
     * @param string $data
     * @return array
     */
    public function suggest(string $type,array $data) :array;

}