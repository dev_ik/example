<?php


namespace app\components\Intake;


class IntakeKladrApiMock implements KladrApiInterface
{

    public const ALLOWED_METHODS = ['zip','city','street'];
    /**
     * @param string $type
     * @param array $data
     * @return array
     */
    public function suggest(string $type,array $data) :array
    {

        if (!in_array($type,self::ALLOWED_METHODS)){
            return [];
        }

        $method = 'suggest'.$type;

        return $this->$method($data);
    }

    /**
     * @param $data
     * @return array
     */
    private function suggestZip($data) :array
    {
        $zip = $data['zip'] ?? '';
        if (!$zip){
            return [];
        }
        return [
            [
                'city' => 'Москва',
                'city_id' => 123,
                'street' => 'улица'
            ]
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function suggestCity($data):array
    {
        $city = $data['city'] ?? '';
        if (!$city) {
            return [];
        }
        return [
            [
                'value' => 'Москва',
                'id' => 123,
                'zip' => 101000
            ]
        ];
    }

    /**
     * @param $data
     * @return array
     */
    private function suggestStreet($data)
    {
        if (!isset($data['city_id']) || !isset($data['street'])) {
            return [];
        }
        return [
            [
                'value' => 'Москва',
                'id' => 123,
                'zip' => 101000
           ]
        ];
    }
}