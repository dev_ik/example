<?php

namespace app\components\Intake;

/**
 * Class IntakeKladrApi
 * @package app\components\module\Intake
 */
class IntakeKladrApi implements KladrApiInterface
{

    public const ALLOWED_METHODS = ['zip','city','street'];
    /**
     * @var string
     */
    public $url;

    /**
     * @param $zip
     * @return mixed
     */
    public function getZipRequest($zip)
    {
        $params = [
            'zip' => $zip,
            'contentType' => 'building',
            'withParent' => 1,
            'limit' => 1,
        ];

        $result = $this->doRequest($params);
        return $result[0] ?? $result;
    }

    /**
     * @param $city
     * @param int $limit
     * @return mixed
     */
    public function getCityRequest($city, $limit = 10)
    {
        $params = [
            'query' => $city,
            'contentType' => 'city',
            'limit' => $limit,
        ];
        $result = $this->doRequest($params);
        return $result;
    }

    /**
     * @param $cityId
     * @param $street
     * @param int $limit
     * @return mixed
     */
    public function getStreetRequest($cityId, $street, $limit = 10)
    {
        $params = [
            'query' => $street,
            'cityId' => $cityId,
            'contentType' => 'street',
            'limit' => $limit,
        ];
        $result = $this->doRequest($params);
        return $result;
    }

    /**
     * @param $cityId
     * @param $streetId
     * @param $house
     * @param int $limit
     * @return mixed
     */
    public function getHouseRequest($cityId, $streetId, $house, $limit = 10)
    {
        $params = [
            'query' => $house,
            'cityId' => $cityId,
            'streetId' => $streetId,
            'contentType' => 'building',
            'limit' => $limit,
        ];
        $result = $this->doRequest($params);
        return $result;
    }

    /**
     * @param $params
     * @return mixed
     */
    private function doRequest($params)
    {
        $url = $this->url;
        $url .= '?' . http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $token = print_r($params, true);
        \Yii::beginProfile($token, __CLASS__ . '::' . $params['contentType']);
        $answer = curl_exec($ch);
        \Yii::endProfile($token, __CLASS__ . '::' . $params['contentType']);
        $result = json_decode($answer, 1);

        return $result['result'] ?? false;
    }

    /**
     * @param string $type
     * @param string $data
     * @return array
     */
    public function suggest(string $type,array $data) :array
    {
        if (!in_array($type,self::ALLOWED_METHODS)){
            return [];
        }

        $method = 'suggest'.$type;

        return $this->$method($data);
    }

    /**
     * @param $zip
     * @return array
     */
    private function suggestZip($data) :array
    {
        $zip = $data['zip'] ?? '';
        if (!$zip || !$res = $this->getZipRequest($zip)){
            return [];
        }

        $found = [];
        foreach ($res['parents'] ?? [] as $parent) {
            if ($parent['contentType'] == 'street') {
                $found['street'] = $parent['typeShort'] . ' ' . $parent['name'];
                $found['street_id'] = $parent['id'];
            }
            if ($parent['contentType'] == 'city') {
                $found['city'] = $parent['name'];
                $found['city_id'] = $parent['id'];
            }
        }
        return $found;
    }

    /**
     * @param $city
     * @return array
     */
    private function suggestCity($data):array
    {
        $city = $data['city'] ?? '';
        if (!$city || !$res = $this->getCityRequest($city)) {
            return [];
        }

        $found = [];
        foreach ($res as $row) {
            $found[] = [
                'value' => $row['name'],
                'id' => $row['id'],
                'zip' => $row['zip'],
            ];
        }
        return $found;
    }

    /**
     * @param $data
     * @return array
     */
    private function suggestStreet($data)
    {
        if (!isset($data['city_id']) || !isset($data['street'])) {
            return [];
        }

        if(!$res = $this->getStreetRequest($data['city_id'], $data['street']))
        {
            return [];
        }

        $found = [];
        $names = [];
        foreach ($res as $row) {
            $name = $row['typeShort'] . ' ' . $row['name'];
            if (in_array($name, $names)) {
                continue;
            }
            $names[] = $name;
            $found[] = [
                'value' => $name,
                'id' => $row['id'],
                'zip' => $row['zip'],
            ];
        }
        return $found;
    }

}
