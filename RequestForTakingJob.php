<?php

namespace app\jobs;

use app\components\C1Exception;
use app\components\globalSetting\setting;
use app\components\NotFoundException;
use app\components\soap\SoapClientInterface;
use app\models\RequestForTaking;
use yii\base\BaseObject;
use yii\db\Expression;
use yii\queue\JobInterface;
use yii\queue\Queue;

class RequestForTakingJob extends BaseObject implements JobInterface
{
    public $requestForTakingId;

    /**
     * @param Queue $queue which pushed and is handling the job
     * @throws \SoapFault
     * @throws \yii\base\InvalidConfigException
     */
    public function execute($queue) :void
    {
        /** @var RequestForTaking $requestForTaking */
        $requestForTaking = RequestForTaking::findOne($this->requestForTakingId);
        if (null === $requestForTaking) {
            throw new NotFoundException(RequestForTaking::class, $this->requestForTakingId);
        }
        $city = $requestForTaking->city;
        $data= [
            'ServiceType' => 0,
            'id' => $requestForTaking->imcode,
            'Address' => $requestForTaking->getAddress($city->Name),
            'ContactPerson' => $requestForTaking->contact_person,
            'ContactPhone' => $requestForTaking->contact_phone,
            'OrderType' => $requestForTaking->taking_type,
            'OrderDate' => $requestForTaking->taking_date,
            'OrderTimeBegin' => $requestForTaking->taking_time_from,
            'OrderTimeEnd' => $requestForTaking->taking_time_to,
            'Quantity' => $requestForTaking->seats_count,
            'Volume' => $requestForTaking->volume,
            'Weight' => $requestForTaking->weight,
            'Comment' => $requestForTaking->comment,
            'CityCode' => $city->CityCode,
            'CargoType' => 9,
            'ArrayOfWaybillEntries' => [],
            'WSRef' =>'',
            'WaybillData'=> ''

        ];

        /** @var SoapClientInterface $client1c */
        $client1c = \Yii::$app->get('client1c');

        $client1c->connect(\Yii::$app->params['soap']['order'], \Yii::$app->params['soap']['soapOptions']);
        $result = $client1c->RequestForServices($data);

        if (empty($result->return->ExitCode)) {
            $requestForTaking->has_error = 1;
            $requestForTaking->error_message = $result->return->Error ?? null;
            $requestForTaking->save(true, ['has_error', 'error_message']);

            throw new C1Exception($client1c, 'Can\'t create request for intake');
        }

        $requestForTaking->has_error = null;
        $requestForTaking->error_message = null;
        $requestForTaking->id_1s = $result->return->ExitCode;
        $requestForTaking->sent_at = new Expression('NOW()');
        $requestForTaking->save(true, ['id_1s', 'send_at', 'has_error', 'error_message']);
    }
}