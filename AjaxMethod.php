<?php

namespace app\components\filters;

use app\components\errorHandler\AjaxErrorHandler;
use yii\base\ActionFilter;
use yii\web\BadRequestHttpException;
use yii\web\Request;
use yii\web\Response;

class AjaxMethod extends ActionFilter
{
    /**
     * @var string the message to be displayed when request isn't ajax
     */
    public $errorMessage = 'Request must be XMLHttpRequest.';

    /**
     * @var Request
     */
    public $request;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        if ($this->request === null) {
            $this->request = \Yii::$app->getRequest();
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws BadRequestHttpException
     * @throws \yii\base\InvalidConfigException
     */
    public function beforeAction($action) : bool
    {
        \Yii::$app->errorHandler->unregister();
        \Yii::$app->set('errorHandler', AjaxErrorHandler::class);
        \Yii::$app->errorHandler->register();
        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($this->request->isAjax) {
            return true;
        }

        throw new BadRequestHttpException($this->errorMessage);
    }
}