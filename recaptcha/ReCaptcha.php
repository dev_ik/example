<?php


namespace app\components\recaptcha;


class ReCaptcha implements ReCaptchaInterface
{
    /** @var string */
    public $secretKey;

    /** @var string */
    public $url;

    /** @var boolean */
    public $active;

    /**
     * @param string $reCaptchaToken
     * @return bool
     */
    public function check(string $reCaptchaToken):bool
    {
        if (!$this->active){
            return true;
        }

        $context = $this->getContext($reCaptchaToken);

        $response = $this->getResponse($context);

        return $response->success;
    }

    /**
     * @param $reCaptchaToken
     * @param $remoteIP
     * @return resource
     */
    private function getContext($reCaptchaToken)
    {
        $data = http_build_query(
            [
                'secret' => $this->secretKey,
                'response' => $reCaptchaToken,

            ]
        );

        $options = [
            'http' =>
                [
                    'method' => 'POST',
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'content' => $data
                ]
        ];

        return stream_context_create($options);
    }

    /**
     * @param $context
     * @return bool|string
     */
    private function getResponse($context) :\stdClass
    {

        $response = file_get_contents(
            $this->url,
            false,
            $context
        );

        return json_decode($response);
    }

}