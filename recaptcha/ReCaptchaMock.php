<?php


namespace app\components\recaptcha;


class ReCaptchaMock implements ReCaptchaInterface
{

    /**
     * @param string $reCaptchaToken
     * @return bool
     */
    public function check(string $reCaptchaToken): bool
    {
        return true;
    }
}