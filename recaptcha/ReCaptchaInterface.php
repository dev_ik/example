<?php


namespace app\components\recaptcha;

/**
 * Interface ReCaptchaInterface
 * @package app\components\recaptcha
 */
interface ReCaptchaInterface
{
    /**
     * @param string $reCaptchaToken
     * @return bool
     */
    public function check(string $reCaptchaToken) : bool;

}