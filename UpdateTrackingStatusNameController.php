<?php
namespace app\commands;

use app\behaviors\oneInstance\OneInstanceBehavior;
use app\components\ConsoleProgress;
use app\models\StatusModel;
use yii\console\Controller;
use yii\helpers\Console;

/**
 * Class UpdateTrackingStatusNameController
 * @package app\commands
 */
class UpdateTrackingStatusNameController extends Controller
{
    /**
     * @var string
     */
    public $url = '';
    /**
     * @var array
     */
    public $data = [];
    /**
     * @var bool
     */
    public $verbose = false;

    /**
     * @var ConsoleProgress
     */
    private $consoleProgress;

    public function behaviors()
    {
        return [
            [
                'class' => OneInstanceBehavior::class,
                'repeatAfterFailure' => true,
            ]
        ];
    }


    public function init()
    {
        $this->url = getenv('TRACKING_URL_UPDATE_STATUS');
        $this->data = [
            'sign' => getenv('TRACKING_SIGN_UPDATE_STATUS'),
            'method' => 'GetParcelStatuses',
        ];
    }

    public function options($actionID)
    {
        return ['verbose'];
    }

    /**
     * @throws \Exception
     */
    public function actionIndex(): void
    {
        $this->print ('Starting update tracking status name');
        \Yii::beginProfile('uts', 'UpdateTrackingStatus::request');
            $response = json_decode($this->sendRequest(),1);
        \Yii::endProfile('uts','UpdateTrackingStatus::request');
        $this->checkError($response);
        $this->updateStatusName($response);
        $this->print("\r\nTracking status name was update");
    }

    /**
     * @param $responseData
     * @throws \Exception
     */
    private function checkError($responseData) :void
    {
        $error = isset($responseData['error']) ? $responseData['error'] : $responseData['errorMessage'];
        if ($error){
            throw new \Exception($error);
        }
    }

    /**
     * @param array $responseData
     * @throws \Exception
     */
    private function updateStatusName(array $responseData):void
    {
        $statuses = $responseData['list']['Status'];
        if ($this->verbose) {
            $this->consoleProgress = new ConsoleProgress(['max' => count($statuses)]);
            $this->consoleProgress->start();
        }
        foreach ($statuses as $status)
        {
            $model = StatusModel::findOrCreate($status['id']);
            $model->name = $status['name'];
            $model->save();
            if (null !== $this->consoleProgress){
                $this->consoleProgress->advance();
            }
        }
    }

    /**
     * @return mixed
     */
    private function sendRequest()
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->data));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $res = curl_exec($ch);
        curl_close($ch);
        return $res;
    }

    /**
     * @param $text
     */
    public function print($text): void
    {
        if ($this->verbose) {
            Console::output($text);
        }
    }

}